import axios from 'axios';
import { useLoaderData } from "react-router-dom";
import Card from "../components/Card";
import { Series } from '../types';

export async function SeriesLoader() {
    try {
        const response = await axios.get("http://localhost:1337/api/series?populate=*", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
        });
        const data: Series = response.data;
        return data;
    } catch (error) {
        throw error;
    }
  }

export default function SeriesPage() {
    const data = useLoaderData() as Series;
    const series = data.data;

    return (
        <main className="page series">
            <h1>Séries</h1>
            <div className='cards'>
                {series.map((serie) => (
                    <Card key={serie.id} data={serie} type="series" />
                ))}
            </div>
        </main>
    )
}
