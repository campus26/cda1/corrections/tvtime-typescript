import axios from 'axios';
import { useLoaderData } from "react-router-dom";
import Card from "../components/Card";
import { AllDatas } from '../types';

export async function AccueilLoader() {
  try {
      const response_films = await axios.get("http://localhost:1337/api/films?populate=*", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
          },
      });
      const response_series = await axios.get("http://localhost:1337/api/series?populate=*", {
          headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
          },
      });
      const data: AllDatas = {
        films: response_films.data,
        series: response_series.data
      };
      return data;
  } catch (error) {
      throw error;
  }
}

export default function Accueil() {
    const data = useLoaderData() as AllDatas;
    const films = data.films.data;
    const series = data.series.data;

    return (
        <main className="page accueil">
          <section>
            <h2>Films populaires</h2>
            <div className="cards">
              {films.map((film) => (
                <Card key={film.id} data={film} type="films" />
              ))}
            </div>
          </section>
          <section>
            <h2>Séries populaires</h2>
            <div className="cards">
              {series.map((serie) => (
                <Card key={serie.id} data={serie} type="series" />
              ))}
            </div>
          </section>

        </main>
    )
}
