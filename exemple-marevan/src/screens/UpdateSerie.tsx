import axios from 'axios';
import { Form, redirect, useLoaderData } from "react-router-dom";
import { Serie, CategoriesData, SerieCategories } from '../types';

export async function UpdateSerieLoader({ params }: { params: { id: string } }) {
    const id = params.id;

    try {
        const response = await axios.get(`http://localhost:1337/api/series/${id}?populate=*`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
        });
        const response_categories = await axios.get(`http://localhost:1337/api/categories`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
        });

        const serie: Serie = response.data.data;
        const categories: CategoriesData = response_categories.data;

        const data: SerieCategories = {
            serie: serie,
            categories: categories
        }
        
        
        return data;
    } catch (error) {
        throw error;
    }
  }

export async function UpdateSerieAction({ request } : { request: Request }) {
    const formData = await request.formData();
    const id = formData.get("id");
    const posterValidation = formData.get("poster") as File;
    const originalPoster = formData.get("originalPoster");

    try {
        let poster = null;

        if (posterValidation.size > 0) {
            const formUpload = new FormData();
            formUpload.append("files", formData.get("poster") as Blob);            

            const uploadFile = await axios.post("http://localhost:1337/api/upload", formUpload, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                }
            }).then((res) => res.data);
            poster = uploadFile[0].id;

            if (originalPoster !== null) {
                await axios.delete(`http://localhost:1337/api/upload/files/${originalPoster}`, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    },
                });
            }
        } else {
            poster = originalPoster;
        }

        await axios.put(`http://localhost:1337/api/series/${id}`, {
            data: {
                title: formData.get("title"),
                description: formData.get("description"),
                nb_of_episodes: formData.get("numberOfEpisodes"),
                score: formData.get("score"),
                categories: formData.getAll("categories"),
                poster: poster
            }
        }, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json",
            },
        });

        return redirect("/home");

    } catch (error) {
        console.error(error);
        return "An error occurred";
    }
}

export default function UpdateSerie() {
    const data = useLoaderData() as SerieCategories;
    const serie = data.serie as Serie;
    const categories = data.categories as CategoriesData;

    return (
        <main className="page update">
            <h1>Modifier une série</h1>
            <Form method="post" className="form" encType="multipart/form-data">
                <input type="hidden" name="id" value={serie.id} />
                <div className="form-element">
                    <label htmlFor="title">Titre</label>
                    <input type="text" name="title" defaultValue={serie.attributes.title} required />
                </div>
                <div className="form-element">
                    <label htmlFor="description">Description</label>
                    <textarea name="description" defaultValue={serie.attributes.description} />
                </div>
                <div className="form-element">
                    <label htmlFor="numberOfEpisodes">Nombre d'épisodes</label>
                    <input 
                        type="number"
                        name="numberOfEpisodes"
                        defaultValue={serie.attributes.nb_of_episodes}
                        min="0"
                        max="999"
                        required
                    />
                </div>
                <div className="form-element">
                    <label htmlFor="score">Score <small>(max 100)</small></label>
                    <input
                        type="number"
                        name="score"
                        defaultValue={serie.attributes.score}
                        min="0"
                        max="100"
                        required
                    />
                </div>
                <div className="form-element">
                    <label htmlFor="categories">Sélectionner une ou plusieurs catégories</label>
                    <div className="categories">
                        {categories.data.map((category) => (
                            <div key={category.id} className="category">
                                <input type="checkbox" name="categories" value={category.id} /> {category.attributes.title}
                            </div>
                        ))}
                    </div>
                </div>

                <div className="form-element posterContainer">
                    <label htmlFor="poster">Affiche</label>
                    <div className="poster">
                        {serie.attributes.poster?.data ? (<>
                            <input type="hidden" name="orginalPoster" value={serie.attributes.poster?.data.id} />
                            <img src={`http://localhost:1337${serie.attributes.poster?.data.attributes.url}`} alt={serie.attributes.title} />
                        </>
                        ) : (
                            <img src="/src/assets/No-Image-Placeholder.svg" alt="Aucune affiche" />
                        )}
                        <input type="file" name="poster" accept="image/*" />
                    </div>
                </div>
                <button type="submit" name="intent" value="updateSerie">Valider</button>
            </Form>
        </main>
    )
}