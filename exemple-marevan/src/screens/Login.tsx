import axios from 'axios';
import { Form, redirect, useNavigate } from "react-router-dom";
import { LoginReponse } from '../types';

export async function LoginAction({ request } : { request: Request }) {
  const formData = await request.formData();

  try {
    const response = await axios.post("http://localhost:1337/api/auth/local", {
      identifier: formData.get("identifier"),
      password: formData.get("password"),
    }, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data: LoginReponse = response.data;

    if (!data.jwt) return "Invalid credentials";

    localStorage.setItem("token", data.jwt);

    return redirect("/home");
  } catch (error) {
    console.error(error);
    return "An error occurred";
  }
}

export default function Login() {
    const navigate = useNavigate();
    
    return (
        <main className="page login">
          <div className="left">
            <img src="logo.png" alt="" />
            <h2>Bienvenue sur TvTime!</h2>
            <p>Ici, vous pourrez ajouter les films et séries que vous avez vus ou souhaitez voir!</p>
          </div>
          <div className="right">
            <h1>Se connecter</h1>
            <Form method="post" className="form">
                <label htmlFor="">Username</label>
                <input type="text" name="identifier" required />
                <label htmlFor="">Mot de passe</label>
                <input type="password" name="password" required />
                <button type="submit">Se connecter</button>
            </Form>
            <small onClick={() => navigate("/register")}>Pas de compte? En créer un</small>
          </div>
        </main>
    )
}
