import { useNavigate } from "react-router-dom";
import { triangle } from "../assets/svgs";

export default function NotFound() {
  const navigate = useNavigate();

  return (
    <main className="page notFound">
      <div className="container">
        <div className="error">
          <span>4</span>
          <span>0</span>
          <span>4</span>
        </div>
        <p>Cette page n'existe pas</p>
        <button onClick={() => {navigate("/home")}}>
          Retourner à l'acceuil
        </button>
        <div className="svgs svgs1">
          {triangle}
          {triangle}
          {triangle}
        </div>
        <div className="svgs svgs2">
          {triangle}
          {triangle}
          {triangle}
        </div>
      </div>
    </main>
  );
}
