import axios from 'axios';
import { Form, redirect, useLoaderData } from "react-router-dom";
import { useState } from 'react';
import { CategoriesData } from '../types';

export async function AddLoader() {
    try {
        const response = await axios.get("http://localhost:1337/api/categories", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
        });        
        const data: CategoriesData = response.data;
        return data;
    } catch (error) {
        throw error;
    }
}

export async function AddAction({ request } : { request: Request }) {
    const formData = await request.formData();
    const intent = formData.get("intent");
    const posterValidation = formData.get("poster") as File;
        
    try {
        let poster;

        if (posterValidation.size > 0) {
            const formUpload = new FormData();
            formUpload.append("files", formData.get("poster") as Blob);            

            const uploadFile = await axios.post("http://localhost:1337/api/upload", formUpload, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                }
            }).then((res) => res.data);
            poster = uploadFile[0].id;
        } else {
            poster = null;
        }
    
        if (intent === "addFilm") {
            await axios.post("http://localhost:1337/api/films", {
                data: {
                    title: formData.get("title"),
                    description: formData.get("description"),
                    duration: formData.get("duration"),
                    score: formData.get("score"),
                    categories: formData.getAll("categories"),
                    poster: poster
                }
            }, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json",
                }
            });

            return redirect("/home");

        } else if (intent === "addSerie") {
            await axios.post("http://localhost:1337/api/series", {
                data: {
                    title: formData.get("title"),
                    description: formData.get("description"),
                    nb_of_episodes: formData.get("numberOfEpisodes"),
                    score: formData.get("score"),
                    categories: formData.getAll("categories"),
                    poster: poster
                }
            }, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                    "Content-Type": "application/json",
                },
            });

            return redirect("/home");
        }
    } catch (error) {
        console.error(error);
        return "An error occurred";
    }
}

export default function Add() {
    const data = useLoaderData() as CategoriesData;
    const categories = data.data;
    
    const [selectedType, setSelectedType] = useState("Film");

    const handleTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        setSelectedType(event.target.value);
    };

    return (
        <main className="page add">
            <h1>Ajouter {selectedType === "Film" ? "un film" : "une série"}</h1>
            <select name="type" id="type" value={selectedType} onChange={handleTypeChange}>
                <option value="Film">Film</option>
                <option value="Série">Série</option>
            </select>
            <Form method="post" className="form" encType="multipart/form-data">
                <div className="form-element">
                    <label htmlFor="title">Titre</label>
                    <input type="text" name="title" required />
                </div>
                <div className="form-element">
                    <label htmlFor="description">Description</label>
                    <textarea name="description"  />
                </div>

                {selectedType === "Film" ? (
                    <div className="form-element">
                        <label htmlFor="duration">Durée (en minutes totales)</label>
                        <input type="number" name="duration" min="0" max="999" required />
                    </div>
                ) : (
                    <div className="form-element">
                        <label htmlFor="numberOfEpisodes">Nombre d'épisodes</label>
                        <input type="number" name="numberOfEpisodes" min="0" max="999" required />
                    </div>
                )}

                <div className="form-element">
                    <label htmlFor="score">Score <small>(max 100)</small></label>
                    <input type="number" name="score" min="0" max="100" required />
                </div>
                <div className="form-element">
                    <label htmlFor="categories">Sélectionner une ou plusieurs catégories</label>
                    <div className="categories">
                        {categories.map((category,) => (
                            <div key={category.id} className="category">
                                <input type="checkbox" name="categories" value={category.id} /> {category.attributes.title}
                            </div>
                        ))}
                    </div>
                </div>
                <div className="form-element">
                    <label htmlFor="poster">Affiche</label>
                    <input type="file" name="poster" accept="image/*" />
                </div>
                
                {selectedType === "Film" ? (
                    <button type="submit" name="intent" value="addFilm">Ajouter le film</button>
                ) : (
                    <button type="submit" name="intent" value="addSerie">Ajouter la série</button>
                )}
            </Form>
        </main>
    )
}