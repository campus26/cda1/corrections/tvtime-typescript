import axios from 'axios';
import { Form, redirect, useNavigate } from "react-router-dom";
import { RegisterReponse } from '../types';

export async function RegisterAction({ request } : { request: Request }) {
  const formData = await request.formData();

  try {
    const response = await axios.post("http://localhost:1337/api/auth/local/register", {
      username: formData.get("username"),
      email: formData.get("email"),
      password: formData.get("password")
    }, {
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data: RegisterReponse = response.data;

    if (!data.jwt) return "Invalid credentials";

    localStorage.setItem("token", data.jwt);

    return redirect("/home");
  } catch (error) {
    console.error(error);
    return "An error occurred";
  }
}

export default function Register() {
    const navigate = useNavigate();
    
    return (
        <main className="page register">
          <div className="left">
            <img src="logo.png" alt="" />
            <h2>Bienvenue sur TvTime!</h2>
            <p>Ici, vous pourrez ajouter les films et séries que vous avez vus ou souhaitez voir!</p>
          </div>
          <div className="right">
            <h1>Créer un compte</h1>
            <Form method="post" className="form">
                <label htmlFor="username">Username</label>
                <input type="text" name="username" required />
                <label htmlFor="email">Email</label>
                <input type="email" name="email" required />
                <label htmlFor="">Mot de passe</label>
                <input type="password" name="password" required />
                <label htmlFor="">Confirmer le mot de passe</label>
                <input type="password" name="passwordConfirmation" required />
                <button type="submit">Créer son compte</button>
            </Form>
            <small onClick={() => navigate("/login")}>Déjà un compte? Se connecter</small>
          </div>
        </main>
    )
}
