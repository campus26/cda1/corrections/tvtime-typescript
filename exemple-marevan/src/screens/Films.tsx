import axios from 'axios';
import { useLoaderData } from "react-router-dom";
import Card from "../components/Card";
import { Films } from '../types';

export async function FilmsLoader() {
    try {
        const response = await axios.get("http://localhost:1337/api/films?populate=*", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
        });
        const data: Films = response.data;
        return data;
    } catch (error) {
        throw error;
    }
  }

export default function FilmsPage() {
    const data = useLoaderData() as Films;
    const films = data.data;

    return (
        <main className="page films">
            <h1>Films</h1>
            <div className='cards'>
                {films.map((film) => (
                    <Card key={film.id} data={film} type="films" />
                ))}
            </div>
        </main>
    )
}
