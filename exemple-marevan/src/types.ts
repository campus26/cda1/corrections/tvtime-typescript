export interface LoginReponse {
    jwt: string
}

export interface RegisterReponse {
    jwt: string
}

export interface CategoriesData {
    data: Category[]
}

export interface Category {
    id: number;
    attributes : {
        title: string,
        description: string,
        films: Films,
        series: Series
    }
}

export interface Film {
    id: number;
    attributes : {
        categories: CategoriesData,
        title: string,
        description: string,
        duration: number,
        score: number,
        poster?: Poster
    }
}
  
export interface Serie {
    id: number;
    attributes : {
        categories: CategoriesData,
        title: string,
        description: string,
        nb_of_episodes: number,
        score: number,
        poster?: Poster
    }
}

export interface Poster {
    data: {
        id: number,
        attributes: {
            url: string
        }
    }
}

export interface AllDatas {
    films: Films,
    series: Series
}

export interface Films {
    data: Film[]
}

export interface Series {
    data: Serie[]
}

export interface TypedData {
    type: string,
    data: {
        data: Film | Serie
    },
    categories: CategoriesData
}

export interface FilmCategories {
    film: Film,
    categories: CategoriesData
}

export interface SerieCategories {
    serie: Serie,
    categories: CategoriesData
}