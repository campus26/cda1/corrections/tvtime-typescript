import axios from 'axios';
import { Form, useNavigate } from "react-router-dom";
import { time, star } from "../assets/svgs";
import { Film, Serie } from '../types';

export async function CardAction({ request } : { request: Request }) {
    const formData = await request.formData();
    const intent = formData.get("intent");
    const id = formData.get("id");

    if (intent === "deleteFilm") {
        try {
            const response = await axios.delete(`http://localhost:1337/api/films/${id}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
            return response;
        } catch (error) {
            throw error;
        }
    } else if (intent === "deleteSerie") {
        try {
            const response = await axios.delete(`http://localhost:1337/api/series/${id}`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            });
            return response;
        } catch (error) {
            throw error;
        }
    }
}

const Card: React.FC<{ data: Film | Serie, type: string }> = ({ data, type } ) => {
    const navigate = useNavigate();

    const handleUpdate = () => {
        navigate(`/update/${type}/${data.id}`);
    }

    const toHoursAndMinutes = (totalMinutes: number) => {
        const hours = Math.floor(totalMinutes / 60);
        const minutes = totalMinutes % 60;

        const formattedMinutes = String(minutes).padStart(2, '0');
      
        return { hours, minutes: formattedMinutes };
    }

    let formatedDuration;

    if ('duration' in data.attributes) {
        formatedDuration = toHoursAndMinutes(data.attributes.duration);
    }

    return (
        <div className="card">
            {data.attributes.poster?.data?.attributes?.url ? (
                <img src={`http://localhost:1337${data.attributes.poster.data.attributes.url}`} />
            ) : (
                <img src="src\assets\No-Image-Placeholder.svg" />
            )}
            <p className="title">{data.attributes.title}</p>
            <div className="infos">
                {'duration' in data.attributes ? (
                    <div>
                        <span>{time}</span>
                        <p>{formatedDuration?.hours} H {}{formatedDuration?.minutes}</p>
                    </div>
                ) : 'nb_of_episodes' in data.attributes ? (
                    <div>
                        <p>{data.attributes.nb_of_episodes} episodes</p>
                    </div>
                ) : (
                    <p>Invalid</p>
                )}
                <div>
                    <span>{star}</span>
                    <p>{data.attributes.score}/100</p>
                </div>
            </div>
            <div className="actions">
                <button onClick={handleUpdate}>Modifier</button>
                {type === "films" ? (
                    <Form className="form_delete" method="delete">
                        <input type="hidden" name="id" value={data.id}></input>
                        <button type="submit" name='intent' value="deleteFilm">Supprimer</button>
                    </Form>
                ) : type === "series" ? (
                    <Form className="form_delete" method="delete">
                        <input type="hidden" name="id" value={data.id}></input>
                        <button type="submit" name='intent' value="deleteSerie">Supprimer</button>
                    </Form>
                ) : (
                    <p>Erreur</p>
                )}
            </div>
        </div>
    )
}

export default Card;