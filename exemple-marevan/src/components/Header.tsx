import { useNavigate } from "react-router-dom";

import { search } from "../assets/svgs"
import { notifications_bell } from "../assets/svgs"

export default function Header() {
    const navigate = useNavigate();

    const logout = () => {
        localStorage.removeItem("token")
        navigate("/login")
    }

    return (
        <header className="header">
            <div className="left">
                <img src="/logo.png" alt="logo" />
                <p>TvTime</p>
            </div>
            <nav>
                <ul>
                    <li className={location.pathname === "/home" ? "active" : ""}>
                        <p onClick={() => {navigate("/home")}}>Accueil</p>
                    </li>
                    <li className={location.pathname === "/films" ? "active" : ""}>
                        <p onClick={() => {navigate("/films")}}>Films</p>
                    </li>
                    <li className={location.pathname === "/series" ? "active" : ""}>
                        <p onClick={() => {navigate("/series")}}>Séries</p>
                    </li>
                    <li className={location.pathname === "/add" ? "active" : ""}>
                        <p onClick={() => {navigate("/add")}}>Ajouter</p>
                    </li>
                    <li>
                        <p onClick={logout}>Déconnexion</p>
                    </li>
                </ul>
            </nav>
            <div  className="right">
                <span>{search}</span>
                <span>{notifications_bell}</span>
            </div>
        </header>
    )
}
