import { Navigate, Outlet } from 'react-router-dom';
import Header from "./Header"
import Footer from "./Footer"

export default function ProtectedRoute() {
    if (!localStorage.getItem('token')) {
        return <Navigate to="/login" replace />
    }

    return (
        <>
            <Header />
            <Outlet/>
            <Footer />
        </>
    )
}
