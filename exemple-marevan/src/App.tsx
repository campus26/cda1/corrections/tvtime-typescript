import { createBrowserRouter, redirect, RouterProvider } from "react-router-dom";

import ProtectedRoute from "./components/ProtectedRoute";
import Accueil, { AccueilLoader } from "./screens/Accueil";
import FilmsPage, { FilmsLoader } from "./screens/Films";
import SeriesPage, { SeriesLoader } from "./screens/Series";
import Login, { LoginAction } from "./screens/Login";
import Register, { RegisterAction } from "./screens/Register";
import Add, { AddLoader, AddAction } from "./screens/Add";

import UpdateSerie, { UpdateSerieLoader, UpdateSerieAction } from "./screens/UpdateSerie";
import UpdateFilm, { UpdateFilmLoader, UpdateFilmAction } from "./screens/UpdateFilm";

import NotFound from "./screens/NotFound";
import { CardAction } from "./components/Card";

const router = createBrowserRouter([
  {
    element: <ProtectedRoute />, // Mes loader sont executés avant le ProtectedRoute? * en parallèle, mais c'est chiant du coup
    loader: () => {
        if (!localStorage.getItem("token")) {
          return redirect("/login");
        }

        return null;
    },
    children: [
      {
        path: "/home",
        element: <Accueil />,
        loader: AccueilLoader,
        action: CardAction
      },
      {
        path: "/films",
        element: <FilmsPage />,
        loader: FilmsLoader,
        action: CardAction
      },
      {
        path: "/series",
        element: <SeriesPage />,
        loader: SeriesLoader,
        action: CardAction
      },
      {
        path: "/add",
        element: <Add />,
        loader: AddLoader,
        action : AddAction
      },
      {
        path: "/update/films/:id",
        element: <UpdateFilm />,
        loader: UpdateFilmLoader,
        action : UpdateFilmAction
      },
      {
        path: "/update/series/:id",
        element: <UpdateSerie />,
        loader: UpdateSerieLoader,
        action : UpdateSerieAction
      }
    ]
  },
  {
    path: "/login",
    element: <Login />,
    action : LoginAction
  },
  {
    path: "/register",
    element: <Register />,
    action : RegisterAction
  },
  {
    path: "*",
    element: <NotFound />
  },
]);

export default function App() {
  
  return (
    <RouterProvider router={router} />
  );
}