/*login */

export interface Pokedex {
  jwt: string;
  user: User;
}

export interface User {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  createdAt: Date;
  updatedAt: Date;
}

export interface Movies {
  id: number;
  attributes: MoviesAttributes[];
}

export interface MoviesAttributes {
  titre: string;
  dateSortie: Date;
  duree: number;
  description: string;
  note: number;
  vue: boolean;
  createdAt: Date;
  updatedAt: Date;
  image: Image;
}

export interface Image {
  data: Data;
}

export interface Data {
  id: number;
  attributes: DataAttributes;
}

export interface DataAttributes {
  name: string;
  alternativeText: null;
  caption: null;
  width: number;
  height: number;
  formats: Formats;
  hash: string;
  ext: EXT;
  mime: MIME;
  size: number;
  url: string;
  previewUrl: null;
  provider: string;
  provider_metadata: null;
  createdAt: Date;
  updatedAt: Date;
}

export enum EXT {
  JPEG = ".jpeg",
  Jpg = ".jpg",
  Webp = ".webp",
}

export interface Formats {
  thumbnail: Large;
  medium?: Large;
  small?: Large;
  large?: Large;
}

export interface Large {
  name: string;
  hash: string;
  ext: EXT;
  mime: MIME;
  path: null;
  width: number;
  height: number;
  size: number;
  url: string;
}

export enum MIME {
  ImageJPEG = "image/jpeg",
  ImageWebp = "image/webp",
}

// Type d'un film
export interface MovieData {
  id: number;
  attributes: {
    titre: string;
    description: string;
    image: {
      data: {
        attributes: {
          url: string;
        };
        id: number;
      };
    };
  };
}

// Type d'une série
export interface SeriesData {
  id: number;
  attributes: {
    titre: string;
    description: string;
    image: {
      data: {
        attributes: {
          url: string;
        };
        id: number;
      };
    };
  };
}

export interface CategoriesData {
  id: number;
  attributes: {
    nom: string;
  };
}

/*Pour la page Update */

export interface files extends File {
  id: number;
}
export interface ActionData {
  title: string;
  dateSortie: string;
  duration: number;
  description: string;
  note: number;
  vue: boolean;
  // episode: number;
  // saison: number;
  image: {
    data: {
      attributes: {
        url: string;
      };
    };
  };
}

export interface LoaderData {
  data: {
    id: number;
    attributes: {
      titre: string;
      dateSortie: string;
      duree: number;
      description: string;
      note: number;
      vue: boolean;
      episode: number;
      saison: number;
      image: {
        data: {
          attributes: {
            url: string;
          };
        };
      };
    };
  };
}

/* composants cards */

export interface CardProps {
  title: string;
  description: string;
  imageUrl: string;
  id_image: number;
}

/* composants cardseries */
export interface CardseriesProps {
  title: string;
  description: string;
  imageUrl: string;
  idimage2: number;
}

/* composants cardcategories */
export interface CardcategoriesProps {
  nom: string;
}

export interface LoaderCategorieData {
  data: {
    nom: string;
    id: number;
    attributes: {
      nom: string;
    };
  };
}

export interface ActionCategoriesData {
  nom: string;
}

/*add serie */
export interface ActionDataSerie {
  title: string;
  dateSortie: string;
  duration: number;
  description: string;
  vue: boolean;
  saison: number;
  episode: number;
}

/*add film */
export interface ActionDataFilm {
  title: string;
  dateSortie: string;
  duration: number;
  description: string;
  vue: boolean;
}

/*add categorie */
export interface ActionDataCategorie {
  nom: string;
}
