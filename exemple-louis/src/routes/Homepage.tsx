import React from "react";
import Cardseries from "../Components/Cardseries";
import { useLoaderData } from "react-router-dom";
import Footer from "../Components/Footer";
import { useNavigate } from "react-router-dom";
import Card from "../Components/Cards";
import Cardcategories from "../Components/Cardscategories";
import Navbar from "../Components/Navbar";
import { MovieData, SeriesData, CategoriesData } from "../utils/Types";

// import {Link} from "react-router-dom";

interface HomeLoaderData {
  movies: { data: MovieData[] };
  series: { data: SeriesData[] };
  categories: { data: CategoriesData[] };
}

export async function Homeloader(): Promise<HomeLoaderData> {
  const token = localStorage.getItem("token");

  const moviesResponse = await fetch(
    "http://localhost:1337/api/films?populate=image",
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  const seriesResponse = await fetch(
    "http://localhost:1337/api/series?populate=image",
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  const categoriesResponse = await fetch(
    "http://localhost:1337/api/categories?populate=image",
    {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  if (!moviesResponse.ok || !seriesResponse.ok || !categoriesResponse.ok) {
    throw new Error("Failed to fetch data.");
  }

  const movies = await moviesResponse.json();
  const series = await seriesResponse.json();
  const categories = await categoriesResponse.json();

  return { movies, series, categories };
}

// async function delete(id){
// deleteFilm(id);
// deleteImage(id)

// }

async function deleteFilm(id: number) {
  const token = localStorage.getItem("token");

  try {
    await fetch(`http://localhost:1337/api/films/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    window.location.href = "/";
  } catch (error) {
    console.error("Erreur lors de la suppression du film :", error);
  }
}

async function deleteImage(id: number) {
  const token = localStorage.getItem("token");

  try {
    await fetch(`http://localhost:1337/api/upload/files/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    window.location.href = "/";
  } catch (error) {
    console.error("Erreur lors de la suppression du film :", error);
  }
}

async function deleteSerie(id: number) {
  const token = localStorage.getItem("token");

  try {
    await fetch(`http://localhost:1337/api/series/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    window.location.href = "/";
  } catch (error) {
    console.error("Erreur lors de la suppression de la serie :", error);
  }
}

async function deleteImageSerie(id: number) {
  const token = localStorage.getItem("token");

  try {
    await fetch(`http://localhost:1337/api/upload/files/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    window.location.href = "/";
  } catch (error) {
    console.error(
      "Erreur lors de la suppression de l'image de la serie :",
      error
    );
  }
}

async function deletecategorie(id: number) {
  const token = localStorage.getItem("token");

  try {
    await fetch(`http://localhost:1337/api/categories/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    window.location.href = "/";
  } catch (error) {
    console.error("Erreur lors de la suppression de la serie :", error);
  }
}

function Homepage() {
  const navigate = useNavigate();

  const {
    movies: films,
    series: seriesData,
    categories: categories,
  } = useLoaderData() as HomeLoaderData;
  // console.log("movies", films.data);

  console.log("categorie", categories.data);
  return (
    <div>
      <h1>Hello</h1>
      <Navbar />
      <div className="image_opacity">
        <img
          src="images/mario.webp"
          alt="Image en pleine largeur"
          className="image-pleine-largeur"
        />
      </div>
      <h1>
        Base de données de <span>Films</span>
      </h1>

      <div className="container1 mb-20 mt-10">
        <h2 className="">
          Bibliothéque de <span>Films</span>
        </h2>
      </div>

      <div className="film">
        {films.data.map((donnees) => {
          if (!donnees.attributes) return;
          return (
            <div className="video-thumbnail" key={donnees.id}>
              <Card
                title={donnees.attributes.titre}
                description={donnees.attributes.description}
                imageUrl={`http://localhost:1337${donnees.attributes.image.data.attributes.url}`}
                id_image={donnees.attributes.image.data.id}
              />

              <div className="space-x-10">
                <button
                  onClick={() => {
                    deleteFilm(donnees.id);
                    deleteImage(donnees.attributes.image.data.id);
                  }}
                  className="btn_supprimer text-white py-2 px-4 rounded mt-4"
                >
                  Supprimer
                </button>
                <button
                  onClick={() => navigate(`/films/${donnees.id}`)}
                  className="btn_modifier py-2 px-4 rounded mt-4 bg-slate-600 text-white"
                >
                  Modifier
                </button>
              </div>
            </div>
          );
        })}
      </div>

      <div className="container2  bg-slate-600 ">
        <h2 className="mb-20 text-white">
          Bibliothéque de <span>Séries</span>
        </h2>
      </div>

      <div className="film bg-slate-600">
        {seriesData.data.map((series) => (
          <div className="video-thumbnail" key={series.id}>
            <Cardseries
              title={series.attributes.titre}
              description={series.attributes.description.slice(0, 255) + "..."}
              imageUrl={`http://localhost:1337${series.attributes.image.data.attributes.url}`}
              idimage2={series.attributes.image.data.id}
            />
            <div className="space-x-10">
              <button
                onClick={() => {
                  deleteSerie(series.id);
                  deleteImageSerie(series.attributes.image.data.id);
                }}
                className="btn_supprimer text-white py-2 px-4 rounded mt-4"
              >
                Supprimer
              </button>
              <button
                onClick={() => navigate(`/series/${series.id}`)}
                className="btn_modifier py-2 px-4 rounded mt-4 bg-slate-600 text-white"
              >
                Modifier
              </button>
            </div>
          </div>
        ))}
      </div>
      <div className="container2  bg-slate-600 ">
        <h2 className="mb-20 text-white">
          Bibliothéque de <span>Catégories</span>
        </h2>
      </div>
      <div className="film bg-slate-600">
        {categories.data.map((categories) => (
          <div className="video-thumbnail" key={categories.id}>
            <Cardcategories nom={categories.attributes.nom} />
            <div className="space-x-10">
              <button
                onClick={() => {
                  deletecategorie(categories.id);
                }}
                className="btn_supprimer text-white py-2 px-4 rounded mt-4"
              >
                Supprimer
              </button>
              <button
                onClick={() => navigate(`/categories/${categories.id}`)}
                className="btn_modifier py-2 px-4 rounded mt-4 bg-slate-600 text-white"
              >
                Modifier
              </button>
            </div>
          </div>
        ))}
      </div>
      <Footer />
    </div>
  );
}

export default Homepage;

// {
//   "data": {
//     "titre": "test2",
//     "dateSortie": "2023-10-18",
//     "duree": 0,
//     "description": "qqsdqsd",
//     "note": 0,
//     "vue": true,
//     "users": 1,
//     "categories": 1

//   }
// }
