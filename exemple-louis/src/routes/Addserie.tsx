import React, { useState } from "react";
import { Form, redirect, useActionData } from "react-router-dom";
import { ActionDataSerie } from "../utils/Types";

export async function AddSerieAction({ request }: { request: Request }) {
  const formData = await request.formData();
  const token = localStorage.getItem("token");
  console.log(token);

  const filesFormData = new FormData();
  if (formData.get("image")) {
    filesFormData.append("files", formData.get("image")!);
  }

  const upload = await fetch("http://localhost:1337/api/upload", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: filesFormData,
  }).then((res) => res.json());
  console.log("filesFormData", filesFormData);

  const data = await fetch("http://localhost:1337/api/series", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: {
        titre: formData.get("titre"),
        dateSortie: formData.get("dateSortie"),
        duree: formData.get("duree"),
        description: formData.get("description"),
        note: formData.get("note"),
        image: upload[0].id,
        vue: true,
        saison: formData.get("saison"),
        episode: formData.get("episode"),
        users: 1,
        categories: 1,
      },
    }),
  }).then((res) => res.json());

  return redirect("/");
}

const Addserie: React.FC = () => {
  const data = useActionData() as ActionDataSerie | null;

  return (
    <div>
      <h2>Ajouter une série</h2>
      {data && (
        <div className="heading">
          <p>Titre: {data.title}</p>
          <p>Date de sortie: {data.dateSortie}</p>
          <p>Durée: {data.duration}</p>
          <p>Description: {data.description}</p>
          <p>Vue: {data.vue ? "Oui" : "Non"}</p>
        </div>
      )}
      <Form method="post" className="form" encType="multipart/form-data">
        <input type="text" name="titre" placeholder="Titre de la serie" />
        <input type="date" name="dateSortie" placeholder="Date de sortie" />
        <input type="number" name="duree" placeholder="Durée" />
        <textarea name="description" placeholder="Description" />
        <input type="number" name="note" placeholder="Note" />
        <input type="number" name="saison" placeholder="saison" />
        <input type="number" name="episode" placeholder="episode" />

        <label>
          Vue
          <input type="checkbox" name="vue" />
        </label>
        <input type="file" name="image" />
        <button className="form-btn" type="submit">
          Ajouter
        </button>
      </Form>
    </div>
  );
};

export default Addserie;
