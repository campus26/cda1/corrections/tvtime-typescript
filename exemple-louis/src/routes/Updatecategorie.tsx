import React from "react";
import {
  Form,
  redirect,
  useActionData,
  useLoaderData,
  LoaderFunctionArgs,
} from "react-router-dom";

import { LoaderCategorieData, ActionCategoriesData } from "../utils/Types";

export async function Updatecategorieloader({ params }: LoaderFunctionArgs) {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(
      `http://localhost:1337/api/categories/${params.id}?populate`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (response.ok) {
      const categorieDetails = await response.json();
      return categorieDetails;
    } else {
      console.error(
        "Erreur lors de la récupération des détails des categories."
      );
      return null;
    }
    return redirect("/");
  } catch (error) {
    console.error("Erreur lors de la requête API :", error);
    return null;
  }
}

export async function UpdateCategorieAction({ request }: { request: Request }) {
  const formData = await (request as Request).formData();
  const token = localStorage.getItem("token") as string;
  const categorieId = formData.get("categorieId") as string;

  const response = await fetch(
    `http://localhost:1337/api/categories/${categorieId}`,
    {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        data: {
          nom: formData.get("nom"),
        },
      }),
    }
  );

  if (response.ok) {
    return redirect("/");
  } else {
    console.error("Erreur lors de la mise à jour des catégories.");
    return "Erreur lors de la mise à jour des catégorie.";
  }
}

const Updatecategorie: React.FC = () => {
  const data = useActionData() as ActionCategoriesData | null;
  const données = useLoaderData() as LoaderCategorieData | null;
  console.log("url", données);

  return (
    <div>
      <h2>Modifier une catégorie</h2>
      {data && (
        <div className="error-message">
          <p>Titre: {data.nom}</p>
        </div>
      )}
      {données && (
        <Form method="post" className="form" encType="multipart/form-data">
          <input type="hidden" name="categorieId" value={données.data.id} />
          <input
            type="text"
            name="nom"
            defaultValue={données.data.attributes.nom}
            placeholder="Titre de la categorie"
          />

          <button className="form-btn" type="submit">
            Modifier
          </button>
        </Form>
      )}
    </div>
  );
};

export default Updatecategorie;
