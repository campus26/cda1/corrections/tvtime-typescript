import React, { useState } from "react";
import { Form, redirect, useActionData } from "react-router-dom";
import { ActionDataCategorie } from "../utils/Types";

export async function AddCategorieAction({ request }: { request: Request }) {
  const formData = await request.formData();
  const token = localStorage.getItem("token");
  console.log(token);

  const data = await fetch("http://localhost:1337/api/categories", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: {
        nom: formData.get("nom"),
        users: 1,
        categories: 1,
      },
    }),
  }).then((res) => res.json());

  return redirect("/");
}

const Addcategorie: React.FC = () => {
  const data = useActionData() as ActionDataCategorie | null;

  return (
    <div>
      <h2>Ajouter une catégorie</h2>
      {data && (
        <div className="heading">
          <p>Titre: {data.nom}</p>
        </div>
      )}
      <Form method="post" className="form" encType="multipart/form-data">
        <input type="text" name="nom" placeholder="Nom de la catégorie" />
        <button className="form-btn" type="submit">
          Ajouter
        </button>
      </Form>
    </div>
  );
};

export default Addcategorie;
