import React from "react";
import {
  Form,
  redirect,
  useActionData,
  useLoaderData,
  LoaderFunctionArgs,
} from "react-router-dom";

import { ActionData, LoaderData, files } from "../utils/Types";

export async function Updatefilmloader({ params }: LoaderFunctionArgs) {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(
      `http://localhost:1337/api/films/${params.id}?populate`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (response.ok) {
      const filmDetails = await response.json();
      return filmDetails;
    } else {
      console.error("Erreur lors de la récupération des détails du film.");
      return null;
    }
    return redirect("/");
  } catch (error) {
    console.error("Erreur lors de la requête API :", error);
    return null;
  }
}

export async function UpdateFilmAction({ request }: { request: Request }) {
  const formData = await (request as Request).formData();
  const token = localStorage.getItem("token") as string;
  const filmId = formData.get("filmId") as string;

  const validationimage = formData.get("image") as File;
  let image: files | undefined;

  if (validationimage.size > 0) {
    const imageFormData = new FormData();
    imageFormData.append("files", formData.get("image") as Blob);

    const imageResponse = await fetch("http://localhost:1337/api/upload", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: imageFormData,
    });

    if (imageResponse.ok) {
      const imageJson = await imageResponse.json();
      image = imageJson[0];
    } else {
      console.error("Erreur lors de l'envoi de la nouvelle image.");
      return "Erreur lors de l'envoi de la nouvelle image.";
    }
  }

  const response = await fetch(`http://localhost:1337/api/films/${filmId}`, {
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: {
        titre: formData.get("titre"),
        dateSortie: formData.get("dateSortie"),
        duree: formData.get("duree"),
        description: formData.get("description"),
        note: formData.get("note"),
        vue: formData.get("vue") === "on",
        image: image ? image.id : undefined,
        users: 1,
        categories: 1,
      },
    }),
  });

  if (response.ok) {
    return redirect("/");
  } else {
    console.error("Erreur lors de la mise à jour du film.");
    return "Erreur lors de la mise à jour du film.";
  }
}

const Updatefilm: React.FC = () => {
  const data = useActionData() as ActionData | null;
  const données = useLoaderData() as LoaderData | null;
  console.log("url", données);

  return (
    <div>
      <h2>Modifier un film</h2>
      {data && (
        <div className="error-message">
          <p>Titre: {data.title}</p>
        </div>
      )}
      {données && (
        <Form method="post" className="form" encType="multipart/form-data">
          <input type="hidden" name="filmId" value={données.data.id} />
          <input
            type="text"
            name="titre"
            defaultValue={données.data.attributes.titre}
            placeholder="Titre du film"
          />
          <input
            type="date"
            name="dateSortie"
            defaultValue={données.data.attributes.dateSortie}
            placeholder="Date de sortie"
          />
          <input
            type="number"
            name="duree"
            defaultValue={données.data.attributes.duree}
            placeholder="Durée"
          />
          <textarea
            name="description"
            defaultValue={données.data.attributes.description}
            placeholder="Description"
          />
          <input
            type="number"
            name="note"
            defaultValue={données.data.attributes.note}
            placeholder="Note"
          />
          <label>
            Vue
            <input
              type="checkbox"
              name="vue"
              defaultChecked={données.data.attributes.vue}
            />
          </label>
          <input type="file" name="image" />

          <button className="form-btn" type="submit">
            Modifier
          </button>
        </Form>
      )}
    </div>
  );
};

export default Updatefilm;
