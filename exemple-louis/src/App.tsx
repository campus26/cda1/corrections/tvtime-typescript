import { useState } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Homepage from "./routes/Homepage";
import { Homeloader } from "./routes/Homepage";
import "./App.css";
import Login from "./routes/Login";
import { LoginAction } from "./routes/Login";
import Protectedroutes from "./Components/ProtectedRoute";
import Addfilm from "./routes/Addfilm";
import Addserie from "./routes/Addserie";
import { AddFilmAction } from "./routes/Addfilm";
import { AddSerieAction } from "./routes/Addserie";
import Updatefilm from "./routes/Updatefilm";
import { UpdateFilmAction, Updatefilmloader } from "./routes/Updatefilm";
import Updateserie from "./routes/Updateserie";
import { Updateserieloader, UpdateSerieAction } from "./routes/Updateserie";
import Addcategorie from "./routes/Addcategorie";
import { AddCategorieAction } from "./routes/Addcategorie";
import Updatecategorie from "./routes/Updatecategorie";
import {
  Updatecategorieloader,
  UpdateCategorieAction,
} from "./routes/Updatecategorie";

const routes = createBrowserRouter([
  {
    element: <Protectedroutes />,
    children: [
      {
        path: "/",
        element: <Homepage />,
        loader: Homeloader,
      },
      {
        path: "ajout",
        element: <Addfilm />,
        action: AddFilmAction,
      },
      {
        path: "/films/:id",
        element: <Updatefilm />,
        loader: Updatefilmloader,
        action: UpdateFilmAction,
      },
      {
        path: "ajoutserie",
        element: <Addserie />,
        action: AddSerieAction,
      },
      {
        path: "/series/:id",
        element: <Updateserie />,
        loader: Updateserieloader,
        action: UpdateSerieAction,
      },
      {
        path: "ajoutcategorie",
        element: <Addcategorie />,
        action: AddCategorieAction,
      },
      {
        path: "/categories/:id",
        element: <Updatecategorie />,
        loader: Updatecategorieloader,
        action: UpdateCategorieAction,
      },
    ],
  },
  {
    path: "/login",
    element: <Login />,
    action: LoginAction,
  },
]);

function App() {
  return <RouterProvider router={routes} />;
}

export default App;
