import React from "react";
import { CardseriesProps } from "../utils/Types";

const Cardseries: React.FC<CardseriesProps> = ({
  title,
  description,
  imageUrl,
  idimage2,
}) => {
  return (
    <div className="bg-slate-600 w-80">
      <img
        src={imageUrl}
        alt={title}
        className="w-80 h-80 object-contain mx-auto"
      />
      <h2 className="text-xl font-semibold mt-4 text-white">{title}</h2>
      <p className=" mt-2">{description}</p>
      <h2 className=" mt-2">{idimage2}</h2>
    </div>
  );
};

export default Cardseries;
