import React from "react";
import { CardcategoriesProps } from "../utils/Types";

const Cardcategories: React.FC<CardcategoriesProps> = ({ nom }) => {
  return (
    <div className="w-80">
      <h2 className="text-xl font-semibold mt-4 text-white ">{nom}</h2>
    </div>
  );
};

export default Cardcategories;
