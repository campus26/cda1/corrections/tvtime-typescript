import React from "react";
import LogoutButton from "./LogoutButton";

const Footer = () => {
  return (
    <footer className="footer bg-slate-600">
      <div className="container_footer">
        <div className="footer-content">
          <div className="container6">
            <div className="footer-logo">
              <img src="images/logo.png" alt="Logo" className="logo" />
            </div>
          </div>
          <div className="container7">
            <LogoutButton/>
          </div>
          <div className="container8">
          <div className="footer-links">
              <ul>
                <li>
                  <a href="/accueil">Accueil</a>
                </li>
                <li>
                  <a href="/ajout">Ajout de film</a>
                </li>
                <li>
                  <a href="/accueil">Ajout Série</a>
                </li>
                <li>
                  <a href="/contact">Contact</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <p>&copy; 2023 Votre Nom. Tous droits réservés.</p>
      </div>
    </footer>
  );
};

export default Footer;
