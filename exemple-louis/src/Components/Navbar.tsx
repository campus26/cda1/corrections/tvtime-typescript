import React from "react";
import "../App.css";

function Navbar() {
  return (
    <nav className="navbar">
      <div className="logo">
        <img src="images/logo.png" alt="Logo" className="logo" />
      </div>
      <ul className="nav-links">
        <li>
          <a href="/accueil">Accueil</a>
        </li>
        <li>
          <a href="/ajout">Ajout Film</a>
        </li>
        <li>
          <a href="/ajoutserie">Ajout Série</a>
        </li>
        <li>
          <a href="/ajoutcategorie">Ajout Catégorie</a>
        </li>
        <li>
          <a href="/contact">Contact</a>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
