import React from "react";

function LogoutButton() {
  const handleLogout = () => {
    // Supprimez le jeton d'authentification du stockage local (localStorage)
    localStorage.removeItem("token");

    // Redirigez l'utilisateur vers la page de connexion ou une autre page appropriée
    window.location.href = "/login"; // Vous pouvez utiliser React Router pour gérer la navigation
  };

  return <button onClick={handleLogout}>DECONNEXION</button>;
}

export default LogoutButton;
