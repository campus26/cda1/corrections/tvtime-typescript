import { Navigate, Outlet } from "react-router-dom";

const ProtectedRoutes = () => {
  if (!localStorage.getItem("token")) {
    return <Navigate to={"/login"} replace />;
  }

  return <Outlet/>;
};

export default ProtectedRoutes;
