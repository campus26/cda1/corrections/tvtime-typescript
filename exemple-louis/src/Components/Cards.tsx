import React from "react";
import { CardProps, CardseriesProps } from "../utils/Types";

const Card: React.FC<CardProps> = ({
  title,
  description,
  imageUrl,
  id_image,
}) => {
  return (
    <div className="bg-white w-80">
      <img
        src={imageUrl}
        alt={title}
        className="w-80 h-80 object-contain mx-auto"
      />
      <h2 className="text-xl font-semibold mt-4">{title}</h2>
      <p className="text-gray-600 mt-2">{description}</p>
      <h2 className="text-xl font-semibold mt-4">{id_image}</h2>
    </div>
  );
};

export default Card;
